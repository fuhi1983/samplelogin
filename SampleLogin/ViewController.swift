//
//  ViewController.swift
//  SampleLogin
//
//  Created by FUJISAWAHIROYUKI on 2015/04/11.
//  Copyright (c) 2015年 sample. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var errorMessageLabel: UILabel!
    @IBOutlet weak var loginBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // 初期設定
        errorMessageLabel.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func login(sender: UIButton) {
        
        UIView.animateWithDuration(0.5, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0.6, options: nil, animations: { () -> Void in
            
                self.loginBtn.bounds.size.width += 20
            
            }) { (finished:Bool) -> Void in
            
                self.errorMessageLabel.text = "ログインに失敗しました"
                self.loginBtn.bounds.size.width -= 20
        }
        
    }
    

}

